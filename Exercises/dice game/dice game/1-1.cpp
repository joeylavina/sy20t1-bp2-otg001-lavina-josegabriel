#include<iostream>
#include<time.h>
#include<string>

using namespace std;

//1-1
int betting(int playergold) {

	int bet = 0;
	do
	{
		
		cout << "Enter valid bet: ";
		cin >> bet;
		cout << endl;
	} 
	 while (bet > playergold);


		return bet;
	
}

//1-2
void diceRoll(int &die1, int &die2)
{
	die1 = rand() % 6 + 1;
	die2 = rand() % 6 + 1;
}


int main(){

	srand(time(NULL));
	int playerGold = 1000;
	bool start = true;

	while (start)
	{
		int dice1;
		int dice2;
		int dice3;
		int dice4;

		diceRoll(dice1, dice2);
		diceRoll(dice3, dice4); 

		cout << "Current gold: " << playerGold << endl;

		int bet = betting(playerGold);

		//1-3
		if (bet != 0 && bet < playerGold || bet == playerGold)
		{
			cout << "Player rolled: " << dice1 << " & " << dice2 << endl;
			int playerRoll = dice1 + dice2; 

			cout << "Opponent rolled: " << dice3 << " & " << dice4 << endl;
			int AIroll = dice3 + dice4;

			if (playerRoll > AIroll && playerRoll != 2)
			{
				cout << "Player won!" << endl << endl; 
				playerGold = playerGold + bet;
			}
			else if (playerRoll < AIroll && AIroll != 2)
			{
				cout << "Opponent won!" << endl << endl;
				playerGold = playerGold - bet;
			}
			else if (playerRoll == 2)
			{
				cout << "Wow! Snake Eyes! Player won!" << endl << endl;
				playerGold = playerGold + (bet * 3);
			}
			else if (AIroll == 2)
			{
				cout << "Unlucky! Opponent won!" << endl << endl;
				playerGold = playerGold - (bet * 3);
			}
			else
			{
				cout << "It's a tie!" << endl << endl	;

			}
		}

		if (playerGold == 0)
		{
			cout << "Game over! You've run out of gold! Better luck next time!" << endl << endl;
		}

	}
	










system("pause");
return 0; 
}
